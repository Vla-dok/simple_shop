--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1
-- Dumped by pg_dump version 14.1

-- Started on 2021-12-13 19:04:34

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE attestation;
--
-- TOC entry 3338 (class 1262 OID 16478)
-- Name: attestation; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE attestation WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Russian_Russia.1251';


ALTER DATABASE attestation OWNER TO postgres;

\connect attestation

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 212 (class 1259 OID 16529)
-- Name: account; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.account (
    id integer NOT NULL,
    first_name character varying(20),
    last_name character varying(20),
    age integer,
    email character varying(30),
    hash_password character varying(100),
    role character varying(30),
    CONSTRAINT account_age_check CHECK (((age > 0) AND (age < 120)))
);


ALTER TABLE public.account OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16528)
-- Name: account_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.account_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_id_seq OWNER TO postgres;

--
-- TOC entry 3339 (class 0 OID 0)
-- Dependencies: 211
-- Name: account_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.account_id_seq OWNED BY public.account.id;


--
-- TOC entry 214 (class 1259 OID 16683)
-- Name: orders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orders (
    id integer NOT NULL,
    customer_id integer,
    quantity integer,
    playconsole_id integer
);


ALTER TABLE public.orders OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16682)
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.orders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orders_id_seq OWNER TO postgres;

--
-- TOC entry 3340 (class 0 OID 0)
-- Dependencies: 213
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.orders_id_seq OWNED BY public.orders.id;


--
-- TOC entry 210 (class 1259 OID 16488)
-- Name: stocks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stocks (
    id integer NOT NULL,
    producer character varying(30),
    model character varying(30),
    price integer,
    quantity integer,
    CONSTRAINT stocks_price_check CHECK ((price > 10000)),
    CONSTRAINT stocks_quantity_check CHECK ((quantity > 0))
);


ALTER TABLE public.stocks OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16487)
-- Name: stocks_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.stocks_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stocks_id_seq OWNER TO postgres;

--
-- TOC entry 3341 (class 0 OID 0)
-- Dependencies: 209
-- Name: stocks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.stocks_id_seq OWNED BY public.stocks.id;


--
-- TOC entry 3177 (class 2604 OID 16532)
-- Name: account id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account ALTER COLUMN id SET DEFAULT nextval('public.account_id_seq'::regclass);


--
-- TOC entry 3179 (class 2604 OID 16686)
-- Name: orders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders ALTER COLUMN id SET DEFAULT nextval('public.orders_id_seq'::regclass);


--
-- TOC entry 3174 (class 2604 OID 16491)
-- Name: stocks id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stocks ALTER COLUMN id SET DEFAULT nextval('public.stocks_id_seq'::regclass);


--
-- TOC entry 3330 (class 0 OID 16529)
-- Dependencies: 212
-- Data for Name: account; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.account (id, first_name, last_name, age, email, hash_password, role) VALUES (6, 'Иван', 'Магомедов', 25, 'ivan@mail.ru', '$2a$10$mBDVp6AEwx0xJ3XGclQCVO.j1qgwT03gItmEbJlZo6It1u6l52MYW', 'CUSTOMER');
INSERT INTO public.account (id, first_name, last_name, age, email, hash_password, role) VALUES (3, 'Владимир', 'Колобов', 27, 'vlako@bk.ru', '$2a$10$KCvbNrtHgStWG.8R2MMDA.PxYLLw1kE7srcsDab9hAaZ8x4x21gxa', 'ADMIN');


--
-- TOC entry 3332 (class 0 OID 16683)
-- Dependencies: 214
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.orders (id, customer_id, quantity, playconsole_id) VALUES (27, 6, 10, 1);
INSERT INTO public.orders (id, customer_id, quantity, playconsole_id) VALUES (28, 3, 10, 1);
INSERT INTO public.orders (id, customer_id, quantity, playconsole_id) VALUES (29, 3, 5, 3);


--
-- TOC entry 3328 (class 0 OID 16488)
-- Dependencies: 210
-- Data for Name: stocks; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.stocks (id, producer, model, price, quantity) VALUES (3, 'Sony', 'PlayStation 5', 50000, 20);
INSERT INTO public.stocks (id, producer, model, price, quantity) VALUES (1, 'Microsoft', 'Xbox Series X', 60000, 26);
INSERT INTO public.stocks (id, producer, model, price, quantity) VALUES (5, 'Sony', 'PSP', 15000, 25);


--
-- TOC entry 3342 (class 0 OID 0)
-- Dependencies: 211
-- Name: account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.account_id_seq', 10, true);


--
-- TOC entry 3343 (class 0 OID 0)
-- Dependencies: 213
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.orders_id_seq', 29, true);


--
-- TOC entry 3344 (class 0 OID 0)
-- Dependencies: 209
-- Name: stocks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.stocks_id_seq', 6, true);


--
-- TOC entry 3183 (class 2606 OID 16535)
-- Name: account account_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT account_pkey PRIMARY KEY (id);


--
-- TOC entry 3185 (class 2606 OID 16688)
-- Name: orders orders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- TOC entry 3181 (class 2606 OID 16495)
-- Name: stocks stocks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stocks
    ADD CONSTRAINT stocks_pkey PRIMARY KEY (id);


--
-- TOC entry 3186 (class 2606 OID 16689)
-- Name: orders orders_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES public.account(id);


--
-- TOC entry 3187 (class 2606 OID 16694)
-- Name: orders orders_playconsole_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_playconsole_id_fkey FOREIGN KEY (playconsole_id) REFERENCES public.stocks(id);


-- Completed on 2021-12-13 19:04:35

--
-- PostgreSQL database dump complete
--

