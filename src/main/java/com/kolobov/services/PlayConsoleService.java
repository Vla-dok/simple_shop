package com.kolobov.services;

import com.kolobov.forms.PlayConsoleForm;
import com.kolobov.modules.PlayConsole;

import java.util.List;

public interface PlayConsoleService {
    void updatePlayConsole(PlayConsoleForm form, Integer playConsoleId);
    void addPlayConsole(PlayConsoleForm form);
    List<PlayConsole> getAllPlayConsole();
    PlayConsole getPlayConsole(Integer playConsoleId);

    void deletePlayConsole(Integer playConsoleId);
}
