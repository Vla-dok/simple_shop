package com.kolobov.services;

import com.kolobov.forms.SignUpForm;
import com.kolobov.modules.User;
import com.kolobov.repositories.UsersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {
    private final PasswordEncoder passwordEncoder;
    private final UsersRepository usersRepository;

    @Override
    public void signUpUser(SignUpForm form) {
        User user = User.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .role(User.Role.CUSTOMER)
                .hashPassword(passwordEncoder.encode(form.getPassword())) //хэшируем пароль, чтобы нельзя было понять, что за пароль
                .age(form.getAge())
                .build();
        usersRepository.save(user);
    }
}