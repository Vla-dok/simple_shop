package com.kolobov.services;

import com.kolobov.forms.OrderForm;
import com.kolobov.forms.UserForm;
import com.kolobov.modules.Order;
import com.kolobov.modules.PlayConsole;
import com.kolobov.modules.User;

import java.util.List;

public interface OrderService {

    List<Order> getAllOrders();
    List<Order> getAllOrdersByUserId(Integer userId);


}
