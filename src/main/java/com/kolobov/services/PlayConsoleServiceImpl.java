package com.kolobov.services;

import com.kolobov.forms.PlayConsoleForm;
import com.kolobov.modules.PlayConsole;
import com.kolobov.repositories.PlayConsolesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class PlayConsoleServiceImpl implements PlayConsoleService {
    private final PlayConsolesRepository playConsolesRepository;

    @Override
    public void deletePlayConsole(Integer playConsoleId) {
        playConsolesRepository.deleteById(playConsoleId);
    }

    @Override
    public void updatePlayConsole(PlayConsoleForm form, Integer playConsoleId) {
        PlayConsole playConsole = PlayConsole.builder()
                .id(playConsoleId)
                .producer(form.getProducer())
                .model(form.getModel())
                .price(form.getPrice())
                .quantity(form.getQuantity())
                .build();
        playConsolesRepository.save(playConsole);

    }

    @Override
    public void addPlayConsole(PlayConsoleForm form) {
        PlayConsole playConsole = PlayConsole.builder()
                .producer(form.getProducer())
                .model(form.getModel())
                .price(form.getPrice())
                .quantity(form.getQuantity())
                .build();
        playConsolesRepository.save(playConsole);

    }

    @Override
    public List<PlayConsole> getAllPlayConsole() {
       return playConsolesRepository.findAll();
    }

    @Override
    public PlayConsole getPlayConsole(Integer playConsoleId) {
        return playConsolesRepository.getPlayConsoleById(playConsoleId);
    }

}
