package com.kolobov.services;

import com.kolobov.forms.SignUpForm;


public interface SignUpService {
    void signUpUser(SignUpForm form);
}
