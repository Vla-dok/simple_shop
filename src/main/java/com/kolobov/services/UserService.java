package com.kolobov.services;

import com.kolobov.forms.UserForm;
import com.kolobov.modules.PlayConsole;
import com.kolobov.modules.User;
import java.util.List;

//в современной практике принято не использовать
//поле private UsersRepository usersRepository напрямую,
//а создавать специальный класс Service
public interface UserService {
    void addUser(UserForm form);
    List<User> getAllUser();

    void deleteUser(Integer userId);

    User getUser(Integer userId);

    void updateUser(UserForm form, Integer userId);

    }
