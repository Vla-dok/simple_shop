package com.kolobov.services;

import com.kolobov.forms.UserForm;
import com.kolobov.modules.PlayConsole;
import com.kolobov.modules.User;
import com.kolobov.repositories.PlayConsolesRepository;
import com.kolobov.repositories.UsersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import java.util.List;

@RequiredArgsConstructor
@Component
public class UserSeviceImpl implements UserService{
    private final PasswordEncoder passwordEncoder;
    private final UsersRepository usersRepository;


//    @Autowired
//    public UserSeviceImpl(UsersRepository usersRepository) {
//        this.usersRepository = usersRepository;
//    } //аннотация @RequiredArgsConstructor самостоятельно создаёт конструктор с полями

    @Override
    public void addUser(UserForm form) {
        User user = User.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .role(User.Role.CUSTOMER)
                .hashPassword(passwordEncoder.encode(form.getPassword())) //хэшируем пароль, чтобы нельзя было понять, что за пароль
                .age(form.getAge())
                .build();
        usersRepository.save(user);
    }

    @Override
    public void deleteUser(Integer userId) {
        usersRepository.deleteById(userId);
    }

@Override
    public void updateUser(UserForm form, Integer userId) {
        User user = User.builder()
                .id(userId)
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .hashPassword(form.getPassword())
                .email(form.getEmail())
                .age(form.getAge())
                .role(form.getRole())
                .build();
        usersRepository.save(user);
    }


    @Override
    public List<User> getAllUser() {
        return usersRepository.findAll();
    }

    @Override
    public User getUser(Integer userId) {
        return usersRepository.getById(userId);
    }



}
