package com.kolobov.services;

import com.kolobov.forms.OrderForm;
import com.kolobov.modules.Order;
import com.kolobov.modules.PlayConsole;
import com.kolobov.modules.User;
import com.kolobov.repositories.OrdersRepository;
import com.kolobov.repositories.PlayConsolesRepository;
import com.kolobov.repositories.UsersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class OrderServiceImpl implements OrderService {
    private final OrdersRepository ordersRepository;

    @Override
    public List<Order> getAllOrders() {
        return ordersRepository.findAll();
    }

    @Override
    public List<Order> getAllOrdersByUserId(Integer userId) {
        return ordersRepository.findAllByCustomer_Id(userId);
    }
}
