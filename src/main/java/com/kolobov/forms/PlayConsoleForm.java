package com.kolobov.forms;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class PlayConsoleForm {
    @NotEmpty
    private String producer;
    @NotEmpty
    private String model;
    @NotEmpty
    private Integer price;
    private Integer quantity;

}
