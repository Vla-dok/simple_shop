package com.kolobov.forms;

import com.kolobov.modules.PlayConsole;
import com.kolobov.modules.User;
import lombok.Data;

import java.util.List;

@Data
public class OrderForm {
    private Integer id;
    private Integer quantity;
    private PlayConsole playConsole;;//private List<PlayConsole> playConsoles;
    private User user;
}
