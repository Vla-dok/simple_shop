package com.kolobov.forms;

import com.kolobov.modules.User;
import lombok.Data;
import org.hibernate.validator.constraints.Length;


import javax.validation.constraints.NotEmpty;


@Data
public class UserForm {
    @NotEmpty
    @Length(max = 30)
    private String firstName;
    @NotEmpty
    @Length(max = 30)
    private String lastName;
    @NotEmpty
    private Integer age;
    @NotEmpty
    private String email;
    @NotEmpty
    private String password;
    @NotEmpty
    private User.Role role;
}
