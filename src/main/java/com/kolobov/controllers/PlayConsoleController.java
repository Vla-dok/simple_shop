package com.kolobov.controllers;

import com.kolobov.forms.PlayConsoleForm;
import com.kolobov.modules.PlayConsole;
import com.kolobov.services.PlayConsoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;


import java.util.List;


@Controller
public class PlayConsoleController {
    @Autowired
    private final PlayConsoleService playConsoleService;

    public PlayConsoleController(PlayConsoleService playConsoleService) {
        this.playConsoleService = playConsoleService;
    }

    @PostMapping("/stocks")
    public String addPlayConsole(PlayConsoleForm form) {
        playConsoleService.addPlayConsole(form);
        return "redirect:/stocks";
    }
    @PostMapping("/stocks/{playConsole-id}/update")
    public String update(PlayConsoleForm form, @PathVariable("playConsole-id") Integer playConsoleId){
        playConsoleService.updatePlayConsole(form, playConsoleId);
        return "redirect:/stocks";
    }
    @GetMapping("/stocks")
    public String getPlayConsolePage(Model model)
    {    List<PlayConsole> stocks = playConsoleService.getAllPlayConsole();
        model.addAttribute("stocks", stocks);
        return "stocks";
    }

    @GetMapping("/stocks/{playConsole-id}")
    public String getUserPage(Model model, @PathVariable("playConsole-id") Integer playConsoleId){
        PlayConsole playConsole = playConsoleService.getPlayConsole(playConsoleId);
        model.addAttribute(playConsole);
        return "stocksUnit";
    }
    @PostMapping("/stocks/{playConsole-id}/delete")
    public String deleteUnit(@PathVariable("playConsole-id") Integer playConsoleId){
        playConsoleService.deletePlayConsole(playConsoleId);
        return "redirect:/stocks";
    }
}
