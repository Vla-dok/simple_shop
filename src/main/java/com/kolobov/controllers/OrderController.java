package com.kolobov.controllers;

import com.kolobov.modules.Order;
import com.kolobov.modules.PlayConsole;
import com.kolobov.modules.User;
import com.kolobov.repositories.OrdersRepository;
import com.kolobov.services.OrderService;
import com.kolobov.services.PlayConsoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class OrderController {
    @Autowired
    private final PlayConsoleService playConsoleService;
    @Autowired
    private final OrderService orderService;
    @Autowired
    private final OrdersRepository ordersRepository;

    public OrderController(PlayConsoleService playConsoleService, OrderService orderService, OrdersRepository ordersRepository) {
        this.playConsoleService = playConsoleService;
        this.orderService = orderService;
        this.ordersRepository = ordersRepository;
    }

    @GetMapping("/orders/{user-id}")
    public String getOrdersPage(Model model, @PathVariable("user-id") Integer userId) {
        List<Order> orders = orderService.getAllOrdersByUserId(userId);
        List<PlayConsole> stocks = playConsoleService.getAllPlayConsole();

        model.addAttribute("orders", orders);
        model.addAttribute("userId", userId);
        model.addAttribute("stocks", stocks);
        return "orders";
    }

    @PostMapping("/orders/{user-id}")
    public String addOrder(@PathVariable("user-id") User userId,
                           @RequestParam("playConsoleId") PlayConsole playConsoleId,
                           @RequestParam("quantity") Integer quantity) {
        Order order = Order.builder()
                .customer(userId)
                .playConsole(playConsoleId)
                .quantity(quantity)
                .build();
        ordersRepository.save(order);
        return "redirect:/orders/{user-id}";
    }
    @GetMapping("/ordersAll")
    public String getAllOrdersPage(Model model)
    {    List<Order> orders = ordersRepository.findAll();
        List<PlayConsole> stocks = playConsoleService.getAllPlayConsole();
        model.addAttribute("orders", orders);
        model.addAttribute("stocks", stocks);
        return "ordersAll";
    }
    @PostMapping("/ordersAll/{order-id}/delete")
    public String deleteOrder(@PathVariable("order-id") Integer orderId){
        ordersRepository.deleteById(orderId);
        return "redirect:/ordersAll";
    }

}
