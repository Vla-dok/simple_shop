package com.kolobov.controllers;

import com.kolobov.forms.UserForm;
import com.kolobov.modules.PlayConsole;
import com.kolobov.modules.User;
import com.kolobov.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class UsersController {

    private final UserService userService;

    @Autowired
    public UsersController(UserService userService) {
        this.userService = userService;
    }
    @GetMapping("/users/{user-id}")
    public String getUserPage(Model model, @PathVariable("user-id") Integer userId){
        User user = userService.getUser(userId);
        model.addAttribute(user);
        return "user";
    }

    @GetMapping("/users")
    public String getUsersPage(Model model)
    {    List<User> users = userService.getAllUser();

        model.addAttribute("users", users);
        return "users";
    }

@PostMapping("/users")
public String addUser(UserForm form){ //аннотация валидатора
    userService.addUser(form);
    return "redirect:/users";
}
@PostMapping("/users/{user-id}/delete")
    public String deleteUser(@PathVariable("user-id") Integer userId){
    userService.deleteUser(userId);
    return "redirect:/users";
}
    @PostMapping("/users/{user-id}/update")
    public String update(UserForm form, @PathVariable("user-id") Integer userId){
        userService.updateUser(form, userId);
        return "redirect:/users";
    }


}
