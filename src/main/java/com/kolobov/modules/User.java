package com.kolobov.modules;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


/**
 * 15.11.2021
 * 20. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder //позволяет создавать пользователей, даже без id
@Entity //это аннотация JPA
@Table(name="account") //указываем, что объект User должен привязываться к таблице account
public class User {
    public enum Role {
        ADMIN, CUSTOMER
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //указываем, что ID генерируется базой данных
    private Integer id;
    private String firstName;
    private String lastName;
    @Column(unique = true)
    private String email;
    private String hashPassword;
    @Column(columnDefinition = "integer not null default 0")
    private Integer age;
    @Enumerated(value = EnumType.STRING) //так значения enum будут выводится вв виде строки
    private Role role;
    @OneToMany(mappedBy = "customer") //означает, что связано с полем owner из Oder
    private List<Order> orders;
    //прописываем в applicaation.properties "spring.jpa.hibernate.ddl-auto=update", чтобы таблица обновлялась автоматически(новая колнка, название)

}
