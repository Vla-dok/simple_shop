package com.kolobov.modules;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder //позволяет создавать объект, даже без id
@Entity
@Table(name="orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //указываем, что ID генерируется базой данных
    private Integer id;
    private Integer quantity;
    @JoinColumn(name = "customer_id")
    @ManyToOne
    private User customer;
    @JoinColumn(name = "playconsole_id")
    @ManyToOne //@ManyToMany
    private PlayConsole playConsole;//List<PlayConsole> playConsoles
 }
