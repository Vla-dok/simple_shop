package com.kolobov.modules;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * 15.11.2021
 * 20. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder //позволяет создавать авто, даже без id
@Entity
@Table(name="stocks")
public class PlayConsole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //указываем, что ID генерируется базой данных
    private Integer id;
    private String producer;
    private String model;
    private Integer price;
    private Integer quantity;
    @OneToMany(mappedBy = "playConsole")//@ManyToMany(mappedBy = "playConsoles")
    private List<Order> orders;//private List<Order> orders
 }
