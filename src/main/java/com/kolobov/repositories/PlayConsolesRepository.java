package com.kolobov.repositories;

import com.kolobov.modules.PlayConsole;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;

public interface PlayConsolesRepository extends JpaRepository<PlayConsole, Integer> {

    List<PlayConsole> findAll();
    PlayConsole getPlayConsoleById(Integer playConsoleid);

}