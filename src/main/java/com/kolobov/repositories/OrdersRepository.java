package com.kolobov.repositories;

import com.kolobov.modules.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrdersRepository extends JpaRepository<Order, Integer> {

    List<Order> findAllByCustomer_Id(Integer userId);



}
