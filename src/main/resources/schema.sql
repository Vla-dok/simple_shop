create table account(
                        id serial primary key,
                        first_name varchar(20),
                        last_name varchar(20),
                        age integer check ( age > 0 and age < 120 ),
                        email varchar(30),
                        hash_password varchar(100),
                        role varchar(30)
);
drop table orders;
create table orders(
                       id serial primary key,
                       customer_id integer,
                       quantity integer,
                       FOREIGN KEY (customer_id) REFERENCES account (id),
                       playConsole_id integer,
                       FOREIGN KEY (playConsole_id) REFERENCES stocks (id)
);
alter table orders drop column имя таблицы;